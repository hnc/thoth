# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the d2d1 library

# THOTH_D2D1_FOUND       - System has d2d1 library
# THOTH_D2D1_LIBRARY     - Library needed to use d2d1


find_library(THOTH_D2D1_LIBRARY NAMES "d2d1")

if (THOTH_D2D1_LIBRARY)
	
	set(THOTH_D2D1_FOUND "TRUE")
	
	link_libraries(${THOTH_D2D1_LIBRARY})
	
	message(STATUS "Library d2d1 found =( ${THOTH_D2D1_LIBRARY}")
	
else()
	
	set(THOTH_D2D1_FOUND "FALSE")
	
	message(STATUS "Library d2d1 not found =)")
	
endif()
