# Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the OSG library

# THOTH_OSG_FOUND       - System has OSG library
# THOTH_OSG_INCLUDE_DIR - The OSG include directory
# THOTH_OSG_LIBRARY     - Library needed to use OSG

# THOTH_WITH_OSG_MACRO  - System has OSG library (macro)


if (THOTH_DISABLE_OSG)
	
	message(STATUS "OSG is disabled")
	set(THOTH_OSG_FOUND FALSE)
	set(THOTH_OSG_INCLUDE_DIR FALSE)
	set(THOTH_OSG_LIBRARY FALSE)
	
else()
	
	find_package(osg)
	find_package(osgDB)
	find_package(osgUtil)
	find_package(osgViewer)
	
	set(THOTH_WITH_OSG_MACRO "thoth_with_OSG")
	
	if (OSG_FOUND AND OSGDB_FOUND AND OSGUTIL_FOUND AND OSGVIEWER_FOUND)
		
		set(THOTH_OSG_FOUND "TRUE")
		set(THOTH_OSG_INCLUDE_DIR "${OSG_INCLUDE_DIR};${OSGDB_INCLUDE_DIR};${OSGUTIL_INCLUDE_DIR};${OSGVIEWER_INCLUDE_DIR}")
		list(REMOVE_DUPLICATES THOTH_OSG_INCLUDE_DIR)
		set(THOTH_OSG_LIBRARY "${OSG_LIBRARIES};${OSGDB_LIBRARIES};${OSGUTIL_LIBRARIES};${OSGVIEWER_LIBRARIES}")
		
		include_directories(${THOTH_OSG_INCLUDE_DIR})
		link_libraries(${THOTH_OSG_LIBRARY})
		
		add_definitions("-D${THOTH_WITH_OSG_MACRO}")
		
		message(STATUS "Library OSG found =) ${THOTH_OSG_INCLUDE_DIR} | ${THOTH_OSG_LIBRARY}")
		
	else()
		
		set(THOTH_OSG_FOUND "FALSE")
		set(THOTH_OSG_INCLUDE_DIR FALSE)
		set(THOTH_OSG_LIBRARY FALSE)
		
		message(STATUS "Library OSG not found :(")
		
	endif()
	
endif()
