# Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the SDL2 library

# THOTH_SDL2_FOUND       - System has SDL2 library
# THOTH_SDL2_INCLUDE_DIR - The SDL2 include directory
# THOTH_SDL2_LIBRARY     - Library needed to use SDL2

# THOTH_WITH_SDL2_MACRO  - System has SDL2 library (macro)


if (THOTH_DISABLE_SDL2)
	
	message(STATUS "SDL2 is disabled")
	set(THOTH_SDL2_FOUND FALSE)
	set(THOTH_SDL2_INCLUDE_DIR FALSE)
	set(THOTH_SDL2_LIBRARY FALSE)
	
else()
	
	find_path(THOTH_SDL2_INCLUDE_DIR "SDL2/SDL.h")
	find_library(THOTH_SDL2_LIBRARY NAMES "SDL2")
	
	set(THOTH_WITH_SDL2_MACRO "thoth_with_SDL2")
	
	if (THOTH_SDL2_LIBRARY AND THOTH_SDL2_INCLUDE_DIR)
		
		set(THOTH_SDL2_FOUND "TRUE")
		
		include_directories(${THOTH_SDL2_INCLUDE_DIR})
		link_libraries(${THOTH_SDL2_LIBRARY})
		
		add_definitions("-D${THOTH_WITH_SDL2_MACRO}")
		
		message(STATUS "Library SDL2 found =) ${THOTH_SDL2_INCLUDE_DIR} | ${THOTH_SDL2_LIBRARY}")
		
	else()
		
		set(THOTH_SDL2_FOUND "FALSE")
		set(THOTH_SDL2_INCLUDE_DIR FALSE)
		set(THOTH_SDL2_LIBRARY FALSE)
		
		message(STATUS "Library SDL2 not found :(")
		
	endif()
	
endif()
