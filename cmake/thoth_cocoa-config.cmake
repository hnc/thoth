# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the Cocoa framework

# THOTH_COCOA_FOUND       - System has Cocoa framework
# THOTH_COCOA_INCLUDE_DIR - The Cocoa include directory
# THOTH_COCOA_LIBRARY     - Library needed to use Cocoa


find_path(THOTH_COCOA_INCLUDE_DIR "CoreGraphics/CGDisplayConfiguration.h")
find_library(THOTH_COCOA_LIBRARY NAMES "Cocoa")

if (THOTH_COCOA_LIBRARY AND THOTH_COCOA_INCLUDE_DIR)
	
	set(THOTH_COCOA_FOUND "TRUE")
	
	include_directories(${THOTH_COCOA_INCLUDE_DIR})
	link_libraries(${THOTH_COCOA_LIBRARY})
	
	message(STATUS "Library Cocoa framework found :( ${THOTH_COCOA_INCLUDE_DIR} | ${THOTH_COCOA_LIBRARY}")
	
else()
	
	set(THOTH_COCOA_FOUND "FALSE")
	
	message(STATUS "Library Cocoa framework not found =)")
	
endif()
