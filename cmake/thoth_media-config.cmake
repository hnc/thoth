# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the Thōth media

# THOTH_MEDIA_FOUND - System has Thōth media
# THOTH_MEDIA_DIR   - Thōth media directory


find_path(THOTH_MEDIA_DIR "share/thoth/README.txt" PATHS ".")

if (THOTH_MEDIA_DIR)
	
	set(THOTH_MEDIA_FOUND "TRUE")
	set(THOTH_MEDIA_DIR "${THOTH_MEDIA_DIR}/share/thoth")
	
	add_definitions("-DTHOTH_MEDIA_DIR=\"${THOTH_MEDIA_DIR}\"")
	
	message(STATUS "Thōth media found =) ${THOTH_MEDIA_DIR}")
	
else()
	
	set(THOTH_MEDIA_FOUND "FALSE")
	
	add_definitions("-DTHOTH_MEDIA_DIR=\"\"")
	
	message(STATUS "Thōth media not found :(")
	
endif()
