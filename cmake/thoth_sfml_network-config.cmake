# Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the SFML NETWORK library

# THOTH_SFML_NETWORK_FOUND       - NETWORK has SFML NETWORK library
# THOTH_SFML_NETWORK_INCLUDE_DIR - The SFML NETWORK include directory
# THOTH_SFML_NETWORK_LIBRARY     - Library needed to use SFML NETWORK

# THOTH_WITH_SFML_NETWORK_MACRO  - NETWORK has SFML NETWORK library (macro)


find_file(THOTH_SFML_CMAKE_DIR "cmake/Modules/FindSFML.cmake")
if (NOT THOTH_SFML_CMAKE_DIR)
	find_file(THOTH_SFML_CMAKE_DIR "SFML/cmake/Modules/FindSFML.cmake")
endif()
if (NOT THOTH_SFML_CMAKE_DIR)
	find_file(THOTH_SFML_CMAKE_DIR "share/SFML/cmake/Modules/FindSFML.cmake")
endif()

if (THOTH_SFML_CMAKE_DIR)
	string(REPLACE "FindSFML.cmake" "" THOTH_SFML_CMAKE_DIR ${THOTH_SFML_CMAKE_DIR})
	set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" "${THOTH_SFML_CMAKE_DIR}")
endif()


find_package(SFML COMPONENTS network)

set(THOTH_WITH_SFML_NETWORK_MACRO "thoth_with_SFML_NETWORK")

if (SFML_FOUND)
	
	set(THOTH_SFML_NETWORK_FOUND "TRUE")
	set(THOTH_SFML_NETWORK_INCLUDE_DIR "${SFML_INCLUDE_DIR}")
	set(THOTH_SFML_NETWORK_LIBRARY "${SFML_LIBRARIES}")
	
	include_directories(${THOTH_SFML_NETWORK_INCLUDE_DIR})
	link_libraries(${THOTH_SFML_NETWORK_LIBRARY})
	
	add_definitions("-D${THOTH_WITH_SFML_NETWORK_MACRO}")
	
	message(STATUS "Library SFML NETWORK found =) ${THOTH_SFML_NETWORK_INCLUDE_DIR} | ${THOTH_SFML_NETWORK_LIBRARY}")
	
else()
	
	set(THOTH_SFML_NETWORK_FOUND "FALSE")
	set(THOTH_SFML_NETWORK_INCLUDE_DIR FALSE)
	set(THOTH_SFML_NETWORK_LIBRARY FALSE)
	
	message(STATUS "Library SFML NETWORK not found :(")
	
endif()
