# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the Thōth library

# THOTH_FOUND       - System has Thōth library
# THOTH_INCLUDE_DIR - The Thōth include directory

# See other lib/thoth_*/*.cmake files for other variables


find_package(hopp REQUIRED)


find_path(THOTH_INCLUDE_DIR "thoth/thoth.hpp")

if (THOTH_INCLUDE_DIR)
	
	set(THOTH_FOUND "TRUE")
	
	include_directories(${THOTH_INCLUDE_DIR})
	
	message(STATUS "Library Thōth found =) ${THOTH_INCLUDE_DIR}")
	
else()
	
	set(THOTH_FOUND "FALSE")
	
	message(STATUS "Library Thōth not found :(")
	
endif()


# find_package(thoth_ogre)
# 
# find_package(thoth_sfml REQUIRED)
# 
# message(STATUS "---")
# if (THOTH_OGRE_FOUND)
# 	set(THOTH_USE_SFML "FALSE")
# 	set(THOTH_USE_OGRE "TRUE")
# else()
# 	set(THOTH_USE_SFML "TRUE")
# 	set(THOTH_USE_OGRE "FALSE")
# endif()
# if (THOTH_USE_OGRE)
# 	message (STATUS "Thōth uses OGRE")
# 	add_definitions("-Dthoth_use_ogre")
# endif()
# if (THOTH_USE_SFML)
# 	message (STATUS "Thōth uses SFML")
# 	add_definitions("-Dthoth_use_sfml")
# endif()
# message (STATUS "Thōth uses SFML")
# add_definitions("-Dthoth_use_sfml")
# 
# find_package(thoth_x11)
# find_package(thoth_cocoa)
# find_package(thoth_d2d1)
# 
# find_package(thoth_media)
