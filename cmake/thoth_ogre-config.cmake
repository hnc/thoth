# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the OGRE library

# THOTH_OGRE_FOUND       - System has OGRE library
# THOTH_OGRE_INCLUDE_DIR - The OGRE include directory
# THOTH_OGRE_LIBRARY     - Library needed to use OGRE

# THOTH_WITH_OGRE_MACRO  - System has OGRE library (macro)


find_file(THOTH_OGRE_CMAKE_DIR "cmake/FindOGRE.cmake")
if (NOT THOTH_OGRE_CMAKE_DIR)
	find_file(THOTH_OGRE_CMAKE_DIR "OGRE/cmake/FindOGRE.cmake")
endif()
if (NOT THOTH_OGRE_CMAKE_DIR)
	find_file(THOTH_OGRE_CMAKE_DIR "lib/OGRE/cmake/FindOGRE.cmake")
endif()

if (THOTH_OGRE_CMAKE_DIR)
	string(REPLACE "FindOGRE.cmake" "" THOTH_OGRE_CMAKE_DIR ${THOTH_OGRE_CMAKE_DIR})
	set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" "${THOTH_OGRE_CMAKE_DIR}")
endif()


if (THOTH_DISABLE_OGRE)
	
	message(STATUS "OGRE is disabled")
	set(THOTH_OGRE_FOUND FALSE)
	set(THOTH_OGRE_INCLUDE_DIR FALSE)
	set(THOTH_OGRE_LIBRARY FALSE)
	
else()
	
	find_package(OGRE)
	
	set(THOTH_WITH_OGRE_MACRO "thoth_with_ogre")
	
	if (OGRE_FOUND)
		
		set(THOTH_OGRE_FOUND "TRUE")
		set(THOTH_OGRE_INCLUDE_DIR "${OGRE_INCLUDE_DIR}")
		set(THOTH_OGRE_LIBRARY "${OGRE_LIBRARIES}")
		
		include_directories(${THOTH_OGRE_INCLUDE_DIR})
		link_libraries(${THOTH_OGRE_LIBRARY})
		
		add_definitions("-D${THOTH_WITH_OGRE_MACRO}")
		
		message(STATUS "Library OGRE found =) ${THOTH_OGRE_INCLUDE_DIR} | ${THOTH_OGRE_LIBRARY}")
		
	else()
		
		set(THOTH_OGRE_FOUND "FALSE")
		set(THOTH_OGRE_INCLUDE_DIR FALSE)
		set(THOTH_OGRE_LIBRARY FALSE)
		
		message(STATUS "Library OGRE not found :(")
		
	endif()
	
endif()