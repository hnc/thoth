# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the X11 library

# THOTH_X11_FOUND       - System has X11 library
# THOTH_X11_INCLUDE_DIR - The X11 include directory
# THOTH_X11_LIBRARY     - Library needed to use X11


find_path(THOTH_X11_INCLUDE_DIR "X11/Xlib.h")
find_library(THOTH_X11_LIBRARY NAMES "X11")

if (THOTH_X11_LIBRARY AND THOTH_X11_INCLUDE_DIR)
	
	set(THOTH_X11_FOUND "TRUE")
	
	include_directories(${THOTH_X11_INCLUDE_DIR})
	link_libraries(${THOTH_X11_LIBRARY})
	
	message(STATUS "Library X11 found =) ${THOTH_X11_INCLUDE_DIR} | ${THOTH_X11_LIBRARY}")
	
else()
	
	set(THOTH_X11_FOUND "FALSE")
	
	message(STATUS "Library X11 not found :(")
	
endif()


# Try to find the Xrandr library

# THOTH_XRANDR_FOUND       - System has Xrandr library
# THOTH_XRANDR_INCLUDE_DIR - The Xrandr include directory
# THOTH_XRANDR_LIBRARY     - Library needed to use Xrandr


find_path(THOTH_XRANDR_INCLUDE_DIR "X11/extensions/Xrandr.h")
find_library(THOTH_XRANDR_LIBRARY NAMES "Xrandr")

if (THOTH_XRANDR_LIBRARY AND THOTH_XRANDR_INCLUDE_DIR)
	
	set(THOTH_XRANDR_FOUND "TRUE")
	
	include_directories(${THOTH_XRANDR_INCLUDE_DIR})
	link_libraries(${THOTH_XRANDR_LIBRARY})
	
	message(STATUS "Library Xrandr found =) ${THOTH_XRANDR_INCLUDE_DIR} | ${THOTH_XRANDR_LIBRARY}")
	
else()
	
	set(THOTH_X11_FOUND "FALSE")
	
	message(STATUS "Library Xrandr not found :(")
	
endif()
