# Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the SDL1 library

# THOTH_SDL1_FOUND       - System has SDL1 library
# THOTH_SDL1_INCLUDE_DIR - The SDL1 include directory
# THOTH_SDL1_LIBRARY     - Library needed to use SDL1

# THOTH_WITH_SDL1_MACRO  - System has SDL1 library (macro)


if (THOTH_DISABLE_SDL1)
	
	message(STATUS "SDL1 is disabled")
	set(THOTH_SDL1_FOUND FALSE)
	set(THOTH_SDL1_INCLUDE_DIR FALSE)
	set(THOTH_SDL1_LIBRARY FALSE)
	
else()
	
	find_path(THOTH_SDL1_INCLUDE_DIR "SDL/SDL.h")
	find_library(THOTH_SDL1_LIBRARY NAMES "SDL")
	
	set(THOTH_WITH_SDL1_MACRO "thoth_with_SDL1")
	
	if (THOTH_SDL1_LIBRARY AND THOTH_SDL1_INCLUDE_DIR)
		
		set(THOTH_SDL1_FOUND "TRUE")
		
		include_directories(${THOTH_SDL1_INCLUDE_DIR})
		link_libraries(${THOTH_SDL1_LIBRARY})
		
		add_definitions("-D${THOTH_WITH_SDL1_MACRO}")
		
		message(STATUS "Library SDL1 found =) ${THOTH_SDL1_INCLUDE_DIR} | ${THOTH_SDL1_LIBRARY}")
		
	else()
		
		set(THOTH_SDL1_FOUND "FALSE")
		set(THOTH_SDL1_INCLUDE_DIR FALSE)
		set(THOTH_SDL1_LIBRARY FALSE)
		
		message(STATUS "Library SDL1 not found :(")
		
	endif()
	
endif()
