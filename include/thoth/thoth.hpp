// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


/*! @mainpage Thōth
 * 
 * https://gitlab.com/hnc/thoth @n
 * http://hnc.toile-libre.org/index.php?section=dev&page=thoth @n
 * https://www.lri.fr/~bagneres/index.php?section=dev&page=thoth
 *
 * @section section_hopp Thōth
 *
 * Modern and generic C++14 header-only library for GUI and games
 * 
 * GNU Affero General Public License 3+
 * 
 * @section section_system_requirement System Requirement
 * 
 * Required:
 * - hopp @n
 * - SFML system audio network @n
 * - SFML window || OpenSceneGraph || OGRE || SDL
 * 
 * @section section_installation Installation
 * 
 * With CMake:
 * - @code
     mkdir build
     cd build
     cmake .. # -DCMAKE_INSTALL_PREFIX=/path/to/install
              #
              # -DCMAKE_BUILD_TYPE=Release
              # -DCMAKE_BUILD_TYPE=Debug
              #
              # -DDISABLE_TESTS=TRUE
              #
              # -DTHOTH_BACKEND=OSG
              # -DTHOTH_BACKEND=OGRE
              # -DTHOTH_BACKEND=SFML_WINDOW
              # -DTHOTH_BACKEND=SDL1
              # -DTHOTH_BACKEND=SDL2
              #
              # -DTHOTH_DISABLE_OSG=1
              # -DTHOTH_DISABLE_OGRE=1
              # -DTHOTH_DISABLE_SFML_WINDOW=1
              # -DTHOTH_DISABLE_SDL1=1
              # -DTHOTH_DISABLE_SDL2=1
     make
     # make doc
     # make test
     make install # su -c "make install" # sudo make install
     @endcode
 * 
 * Without CMake:
 * - This project is a header-only library, you can copy the include directory in /usr/local (for example) or in your project. (But you have to define some macros to enable optional parts.)
 * 
 * @section section_utilization Utilization
 * 
 * If you use CMake, add these lines in your CMakeLists.txt:
 * @code
   # Thōth
   message(STATUS "---")
   find_package(thoth REQUIRED)
   # See /installation/path/lib/thoth/thoth-config.cmake for CMake variables
   @endcode
 */


#ifndef THOTH_HPP
#define THOTH_HPP

/**
 * @defgroup thoth_about About Thōth
 * @copydoc thoth
 */

#include <string>


/**
 * @brief Thōth
 * 
 * @code
   #include <thoth/thoth.hpp>
   @endcode
 */
namespace thoth
{
	/// @brief Version of Thōth
	/// @return version of Thōth
	/// @ingroup thoth_about
	inline std::string version() { return "0.0.0"; }
	
	/// @brief Codename of Thōth
	/// @return codename of Thōth
	/// @ingroup thoth_about
	inline std::string codename() { return "A New Way"; }
}

#endif
