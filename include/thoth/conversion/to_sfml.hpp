// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_CONVERSION_TO_SFML_HPP
#define THOTH_CONVERSION_TO_SFML_HPP

#include <hopp/container/vector2.hpp>
#include <hopp/geometry/rectangle.hpp>
#include <hopp/int/uint8.hpp>
#include <hopp/color.hpp>

#ifdef thoth_with_SFML_WINDOW
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#endif


namespace thoth
{
	#ifdef thoth_with_SFML_WINDOW
	
	/// @brief hopp::vector2<T> to sf::Vector2<T>
	/// @param[in] vector2 A hopp::vector2<T>
	/// @return sf::Vector2<T> from hopp::vector2<T>
	template <class T>
	sf::Vector2<T> to_sfml(hopp::vector2<T> const & vector2)
	{
		return sf::Vector2<T>(vector2.x, vector2.y);
	}
	
	/// @brief hopp::rectangle<T> to sf::Rect<T>
	/// @param[in] rectangle A hopp::rectangle<T>
	/// @return sf::Rect<T> from hopp::rectangle<T>
	template <class T>
	sf::Rect<T> to_sfml(hopp::rectangle<T> const & rectangle)
	{
		return sf::Rect<T>(rectangle.left, rectangle.top, rectangle.width, rectangle.height);
	}
	
	/// @brief hopp::uint8 to sf::Uint8
	/// @param[in] uint8 A hopp::uint8
	/// @return sf::Uint8 from hopp::uint8
	inline sf::Uint8 to_sfml(hopp::uint8 const & uint8)
	{
		return sf::Uint8(uint8.i);
	}
	
	/// @brief hopp::color to sf::Color
	/// @param[in] color A hopp::color
	/// @return sf::Color from hopp::color
	inline sf::Color to_sfml(hopp::color const & color)
	{
		return sf::Color(thoth::to_sfml(color.r), thoth::to_sfml(color.g), thoth::to_sfml(color.b), thoth::to_sfml(color.a));
	}
	
	#endif
}

#endif
