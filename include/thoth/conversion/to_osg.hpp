// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_CONVERSION_TO_OSG_HPP
#define THOTH_CONVERSION_TO_OSG_HPP

#include <hopp/color.hpp>

#ifdef thoth_with_OSG
#include <osg/Vec4f>
#endif


namespace thoth
{
	#ifdef thoth_with_OSG
	
	/// @brief hopp::color to osg::Vec4
	/// @param[in] color A hopp::color
	/// @return osg::Vec4 from hopp::color
	inline osg::Vec4 to_osg(hopp::color const & color)
	{
		return { float(color.r) / 255.f, float(color.g) / 255.f, float(color.b) / 255.f, 1.f };
	}
	
	#endif
}

#endif
