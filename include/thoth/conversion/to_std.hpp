// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_CONVERSION_TO_STD_HPP
#define THOTH_CONVERSION_TO_STD_HPP

#include <chrono>
#include <cstdint>

#ifdef thoth_with_sfml
#include <SFML/System/Time.hpp>
#endif


namespace thoth
{
	#ifdef thoth_with_sfml
	
	/// @brief sf::Time to std::chrono::duration<int64_t, std::micro>
	/// @param[in] time A sf::Time
	/// @return std::chrono::duration<int64_t, std::micro> from sf::Time
	inline std::chrono::duration<int64_t, std::micro> to_std(sf::Time const & time)
	{
		return std::chrono::duration<int64_t, std::micro>(time.asMicroseconds());
	}
	
	#endif
}

#endif
