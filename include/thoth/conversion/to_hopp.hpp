// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_CONVERSION_TO_HOPP_HPP
#define THOTH_CONVERSION_TO_HOPP_HPP

#include <hopp/container/vector2.hpp>
#include <hopp/geometry/rectangle.hpp>

#ifdef thoth_with_SFML_WINDOW
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#endif


namespace thoth
{
	#ifdef thoth_with_SFML_WINDOW
	
	/// @brief sf::Vector2<T> to hopp::vector2<T>
	/// @param[in] vector2 A sf::Vector2<T>
	/// @return hopp::vector2<T> from sf::Vector2<T>
	template <class T>
	hopp::vector2<T> to_hopp(sf::Vector2<T> const & vector2)
	{
		return { vector2.x, vector2.y };
	}
	
	/// @brief sf::Rect<T> to hopp::rectangle<T>
	/// @param[in] rectangle A sf::Rect<T>
	/// @return hopp::rectangle<T> from sf::Rect<T>
	template <class T>
	hopp::rectangle<T> to_hopp(sf::Rect<T> const & rectangle)
	{
		return { rectangle.left, rectangle.top, rectangle.width, rectangle.height };
	}
	
	#endif
}

#endif
