// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_WINDOW_HPP
#define THOTH_WINDOW_HPP

/**
 * @defgroup thoth_window Window
 *
 * @brief Window
 *
 * @code
   #include <thoth/window.hpp>
   @endcode
 */

// Window
#include "window/window.hpp"

// // Texture & Sprites
// #include "window/texture.hpp"
// #include "window/sprite.hpp"
// 
// // Text
// #include "window/text.hpp"
// 
// // Primitives
// #include "window/primitives.hpp"
// 
// // Mouse & Keyboard
// #include "window/mouse.hpp"
// #include "window/keyboard.hpp"

/**
 * @defgroup thoth_window_osg OSG window
 *
 * @brief OSG window
 *
 * @code
   #include <thoth/window.hpp>
   @endcode
 *
 * @ingroup thoth_window
 */

/**
 * @defgroup thoth_window_sfml SFML window
 *
 * @brief SFML window
 *
 * @code
   #include <thoth/window.hpp>
   @endcode
 *
 * @ingroup thoth_window
 */

#endif
