// Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_WINDOW_OSG_WINDOW_HPP
#define THOTH_WINDOW_OSG_WINDOW_HPP

#ifdef thoth_with_OSG

// #include <SFML/Graphics.hpp>
// 
// #include <hopp/container/vector2.hpp>
// 
// #include "../../conversion/to_hopp.hpp"
// #include "../../conversion/to_sfml.hpp"

#include <memory>

#include <hopp/memory.hpp>
#include <hopp/color.hpp>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include "../../conversion/to_osg.hpp"

#include "../T/window_style.hpp"


namespace thoth
{
	namespace osg
	{
		/**
		 * @brief OSG window
		 *
		 * @code
		   #include <thoth/window.hpp>
		   @endcode
		 *
		 * @ingroup thoth_window_osg
		 */
		class window
		{
		private:
			
			/// OSG viewer
			::osgViewer::Viewer m_viewer;
			
			/// OSG traits
			hopp::view_ptr<::osg::GraphicsContext::Traits> m_traits;
			
			/// OSG graphics context
			hopp::view_ptr<::osg::GraphicsContext> m_graphics_context;
			
			/// OSG window
			hopp::view_ptr<::osgViewer::GraphicsWindow> m_window;
			
			/// OSG camera
			hopp::view_ptr<::osg::Camera> m_camera;
			
			/// Title
			std::string m_title;
			
			/// Size
			hopp::vector2<hopp::uint> m_size;
			
			/// Style
			thoth::window_style m_style;
			
		public:
			
			/// @brief Constructor
			/// @param[in] title Window title
			/// @param[in] size  Window size
			window
			(
				std::string const & title = "A thoth::window :)",
				hopp::vector2<hopp::uint> const & size = { 800u, 600u }
			) :
				window(title, size, thoth::window_style())
			{ }
			
			/// @brief Constructor
			/// @param[in] title      Window title
			/// @param[in] size       Window size
			/// @param[in] style_args thoth::window_style arguments
			template <class ... style_args_t>
			window
			(
				std::string const & title,
				hopp::vector2<hopp::uint> const & size,
				style_args_t const & ... style_args
			) :
				m_viewer(),
				m_graphics_context(),
				m_window(),
				m_camera(),
				m_title(title),
				m_size(size),
				m_style(thoth::window_style(style_args...))
			{
				// http://trac.openscenegraph.org/projects/osg//browser/OpenSceneGraph/trunk/examples/osgcamera/osgcamera.cpp
				
				//m_viewer.addEventHandler(new osgViewer::StatsHandler);
				//m_viewer.addEventHandler(new osgViewer::ThreadingHandler);
				
				// Traits
				
				::osg::ref_ptr<::osg::GraphicsContext::Traits> traits =
					new ::osg::GraphicsContext::Traits;
				
				traits->windowName = m_title;
				//traits->x = 0;
				//traits->y = 0;
				traits->width = int(m_size.x);
				traits->height = int(m_size.y);
				traits->windowDecoration = m_style.has_decoration();
				traits->doubleBuffer = true;
				traits->sharedContext = 0;
				
				m_traits = *traits;
				
				// GraphicsContext
				
				::osg::ref_ptr<::osg::GraphicsContext> graphics_context =
					::osg::GraphicsContext::createGraphicsContext(traits.get());
				graphics_context->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				
				m_graphics_context = *graphics_context.get();
				
				// Camera
				
				::osg::ref_ptr<::osg::Camera> camera = new ::osg::Camera;
				
				camera->setGraphicsContext(graphics_context.get());
				//camera->setViewport(new osg::Viewport((i*width)/numCameras,(i*height)/numCameras, width/numCameras, height/numCameras)); // i = camera index
				GLenum buffer = traits->doubleBuffer ? GL_BACK : GL_FRONT;
				camera->setDrawBuffer(buffer);
				camera->setReadBuffer(buffer);
				
				m_camera = *camera.get();
				
				// Window
				
				m_window = *dynamic_cast<::osgViewer::GraphicsWindow *>(graphics_context.get());
				if (m_window == nullptr) { std::cerr << "Error: thoth::osg::window: can not get m_window" << std::endl; }
				
				// m_viewer
				
				m_viewer.addSlave
				(
					camera.get(),
					::osg::Matrixd(),
					::osg::Matrixd::scale(1.0, 1.0, 1.0)
				);
				
				m_viewer.getCamera()->setViewport(new ::osg::Viewport(0, 0, int(m_size.x), int(m_size.y))); 
				
				m_viewer.realize();
			}
			
// 			/// @brief Get the OSG window
// 			/// @return the OSG window
// 			/// @warning If you use this function because Thōth has a missing feature, please send a bug report
// 			std::unique_ptr<::osg::GraphicsContext::WindowingSystemInterface> const & window_osg_() const { return m_viewer; }
// 			
// 			/// @brief Get the OSG window
// 			/// @return the OSG window
// 			/// @warning If you use this function because Thōth has a missing feature, please send a bug report
// 			std::unique_ptr<::osg::GraphicsContext::WindowingSystemInterface> & window_osg_() { return m_viewer; }
			
			// Title, Size, Position, Style
			
			/// Get title
			/// @return title
			std::string const & title() const { return m_title; }
			
			/// Set title
			/// @param[in] title Title
			void set_title(std::string const & title)
			{
				m_title = title;
				// TODO
				// std::cout << "m_window->getWindowName() = " << m_window->getWindowName() << std::endl; // segfault
			}
			
			/// Get size
			/// @return size
			hopp::vector2<hopp::uint> const & size() const { return m_size; }
			
			/// Set size
			/// @param[in] size Size
			void set_size(hopp::vector2<hopp::uint> const & size)
			{
				m_size = size;
				m_graphics_context->getWindowingSystemInterface()->setScreenResolution(0, int(m_size.x), int(m_size.y)); // TODO You must build osgViewer with Xrandr 1.2 or higher for setScreenSettings support!
			}
		
			/// @brief Get position
			/// @return position
			hopp::vector2<int> position() const
			{
				// TODO
				return { 0, 0 };
			}
			
			/// @brief Set position
			/// @param[in] position A hopp::vector2<int>
			void set_position(hopp::vector2<int> const & position)
			{
				// TODO
			}
			
			/// @brief Get style
			/// @return style
			thoth::window_style style() const { return m_style; }
			
			// Open, Close, Clear, Update
			
			/// @brief Window is open?
			/// @return true is the window is open, false otherwise
			bool is_open() const { return m_viewer.done() == false; }
			
			/// @brief Close the window
			void close() { m_viewer.setDone(true); }
			
			/// @brief Clear frame
			/// @param[in] background_color Background color (hopp::color::black() by default)
			void clear(hopp::color const & background_color = hopp::color::black())
			{
				m_graphics_context->setClearColor(thoth::to_osg(background_color));
				//m_graphics_context->clear();
			}
			
			/// @brief Update window (draw frame)
			void update() { m_viewer.frame(); }
		};
	}
}

#endif

#endif
