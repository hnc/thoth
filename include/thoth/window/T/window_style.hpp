// Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_WINDOW_T_WINDOW_STYLE_HPP
#define THOTH_WINDOW_T_WINDOW_STYLE_HPP

#include <string>

#include <hopp/container/vector2.hpp>
#include <hopp/int/uint.hpp>


namespace thoth
{
	/**
	 * @brief Window style
	 *
	 * @code
	   #include <thoth/window.hpp>
	   @endcode
	 *
	 * @ingroup thoth_window
	 */
	class window_style
	{
	private:
		
		/// Fullscreen
		bool m_fullscreen;
		
		// Can be resized
		bool m_resizeable;
		
		// Can be closed
		bool m_closeable;
		
		// Has decoration
		bool m_decoration;
		
	public:
		
		/// Default constructor
		/// @param[in] fullscreen true if fullscreen, false otherwise
		/// @param[in] resizeable true if resizeable, false otherwise
		/// @param[in] closeable  true if closeable, false otherwise
		/// @param[in] decoration true if window has decoration, false otherwise
		window_style
		(
			bool const fullscreen = false,
			bool const resizeable = true,
			bool const closeable = true,
			bool const decoration = true
		) :
			m_fullscreen(fullscreen),
			m_resizeable(resizeable),
			m_closeable(closeable),
			m_decoration(decoration)
		{ }
		
		// Getters
		
		/// Fullscreen?
		/// @return true if fullscreen, false otherwise
		bool is_fullscreen() const { return m_fullscreen; }
		
		///  Can be resized?
		/// @return true if resizeable, false otherwise
		bool is_resizeable() const { return m_resizeable; }
		
		/// Can be closed?
		/// @return true if closeable, false otherwise
		bool is_closeable() const { return m_closeable; }
		
		/// Has decoration?
		/// @return true if decoration, false otherwise
		bool has_decoration() const { return m_decoration; }
		
		// Setters
		
		/// Fullscreen
		/// @param[in] b true if fullscreen, false otherwise
		/// @return current thoth::window_style to chain setters
		window_style & fullscreen(bool const b) { m_fullscreen = b; return *this; }
		
		/// Can be resized
		/// @param[in] b true if resizeable, false otherwise
		/// @return current thoth::window_style to chain setters
		window_style & resizeable(bool const b) { m_resizeable = b; return *this; }
		
		/// Can be closed
		/// @param[in] b true if closeable, false otherwise
		/// @return current thoth::window_style to chain setters
		window_style & closeable(bool const b) { m_closeable = b; return *this; }
		
		/// Decoration
		/// @param[in] b true if window has decoration, false otherwise
		/// @return current thoth::window_style to chain setters
		window_style & decoration(bool const b) { m_decoration = b; return *this; }
	};
}

#endif
