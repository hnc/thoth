// Copyright © 2015, 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_WINDOW_SFML_WINDOW_HPP
#define THOTH_WINDOW_SFML_WINDOW_HPP

#ifdef thoth_with_SFML_WINDOW

#include <hopp/color.hpp>
#include <hopp/container/vector2.hpp>

#include <SFML/Graphics.hpp>

#include "../../conversion/to_hopp.hpp"
#include "../../conversion/to_sfml.hpp"

#include "../T/window_style.hpp"


namespace thoth
{
	namespace sfml
	{
		/**
		 * @brief SFML window
		 *
		 * @code
		   #include <thoth/window.hpp>
		   @endcode
		 *
		 * @ingroup thoth_window_sfml
		 */
		class window
		{
		private:
			
			/// Window
			sf::RenderWindow m_window;
			
			/// Title
			std::string m_title;
			
			/// Size
			hopp::vector2<hopp::uint> m_size;
			
			/// Style
			thoth::window_style m_style;
			
		public:
			
			/// @brief Constructor
			/// @param[in] title Window title
			/// @param[in] size  Window size
			window
			(
				std::string const & title = "A thoth::window :)",
				hopp::vector2<hopp::uint> const & size = { 800u, 600u }
			) :
				window(title, size, thoth::window_style())
			{ }
			
			/// @brief Constructor
			/// @param[in] title      Window title
			/// @param[in] size       Window size
			/// @param[in] style_args thoth::window_style arguments
			template <class ... style_args_t>
			window
			(
				std::string const & title,
				hopp::vector2<hopp::uint> const & size,
				style_args_t const & ... style_args
			) :
				m_window(),
				m_title(title),
				m_size(size),
				m_style(thoth::window_style(style_args...))
			{
				m_window.create
				(
					{ m_size.x, m_size.y },
					m_title,
					hopp::uint
					(
						m_style.has_decoration() == false
						?
							sf::Style::None
						:
							sf::Style::Titlebar |
							(m_style.is_fullscreen() ? sf::Style::Fullscreen : 0) |
							(m_style.is_resizeable() ? sf::Style::Resize : 0) |
							(m_style.is_closeable() ? sf::Style::Close : 0)
					)
				);
				
				m_window.setVerticalSyncEnabled(true);
			}
			
			/// @brief Get the SFML window
			/// @return the SFML window
			/// @warning If you use this function because Thōth has a missing feature, please send a bug report
			sf::RenderWindow const & window_sfml_() const { return m_window; }
			
			/// @brief Get the SFML window
			/// @return the SFML window
			/// @warning If you use this function because Thōth has a missing feature, please send a bug report
			sf::RenderWindow & window_sfml_() { return m_window; }
			
			// Title, Size, Position, Style
			
			/// Get title
			/// @return title
			std::string const & title() const { return m_title; }
			
			/// Set title
			/// @param[in] title Title
			void set_title(std::string const & title)
			{
				m_title = title;
				m_window.setTitle(m_title);
			}
			
			/// Get size
			/// @return size
			hopp::vector2<hopp::uint> const & size() const { return m_size; }
			
			/// Set size
			/// @param[in] size Size
			void set_size(hopp::vector2<hopp::uint> const & size)
			{
				m_size = size;
				m_window.setSize(thoth::to_sfml(m_size));
			}
			
			/// @brief Get position
			/// @return position
			hopp::vector2<int> position() const
			{
				return thoth::to_hopp(m_window.getPosition());
			}
			
			/// @brief Set position
			/// @param[in] position A hopp::vector2<int>
			void set_position(hopp::vector2<int> const & position)
			{
				m_window.setPosition(thoth::to_sfml(position));
			}
			
			/// @brief Get style
			/// @return style
			thoth::window_style style() const { return m_style; }
			
			// Open, Close, Clear, Update
			
			/// @brief Window is open?
			/// @return true is the window is open, false otherwise
			bool is_open() const { return m_window.isOpen(); }
			
			/// @brief Close the window
			void close() { m_window.close(); }
			
			/// @brief Clear frame
			/// @param[in] background_color Background color (hopp::color::black() by default)
			void clear(hopp::color const & background_color = hopp::color::black())
			{
				m_window.clear(thoth::to_sfml(background_color));
			}
			
			/// @brief Update window (draw frame)
			void update() { m_window.display(); }
		};
	}
}

#endif

#endif
