// Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of Thōth.

// Thōth is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Thōth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Thōth. If not, see <http://www.gnu.org/licenses/>


#ifndef THOTH_WINDOW_WINDOW_HPP
#define THOTH_WINDOW_WINDOW_HPP

#include "osg/window.hpp"
#include "sfml/window.hpp"

namespace thoth
{
	#ifdef THOTH_BACKEND_OSG
	using window = thoth::osg::window;
	#endif
	
	#ifdef THOTH_BACKEND_SFML_WINDOW
	using window = thoth::sfml::window;
	#endif
	
	#ifdef DOXYGEN
	/**
	 * @brief A typedef on thoth::osg::window or thoth::sfml::window
	 *
	 * @code
	   #include <thoth/window.hpp>
	   @endcode
	 *
	 * @ingroup thoth_window
	 */
	class window { };
	#endif
}

#endif
