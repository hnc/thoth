# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

https://gitlab.com/hnc/thoth
http://hnc.toile-libre.org/index.php?section=dev&page=thoth
https://www.lri.fr/~bagneres/index.php?section=dev&page=thoth


 -------
| Thōth |
 -------

Modern and generic C++14 header-only library for GUI and games

GNU Affero General Public License 3+


 --------------------
| System Requirement |
 --------------------

Required:
- C++14 compiler
- hopp
- SFML system audio network
- SFML window || OpenSceneGraph || OGRE || SDL


 --------------
| Installation |
 --------------
 
With CMake
----------

mkdir build
cd build
cmake .. # -DCMAKE_INSTALL_PREFIX=/path/to/install
         #
         # -DCMAKE_BUILD_TYPE=Release
         # -DCMAKE_BUILD_TYPE=Debug
         #
         # -DDISABLE_TESTS=TRUE
         #
         # -DTHOTH_BACKEND=OSG
         # -DTHOTH_BACKEND=OGRE
         # -DTHOTH_BACKEND=SFML_WINDOW
         # -DTHOTH_BACKEND=SDL1
         # -DTHOTH_BACKEND=SDL2
         #
         # -DTHOTH_DISABLE_OSG=1
         # -DTHOTH_DISABLE_OGRE=1
         # -DTHOTH_DISABLE_SFML_WINDOW=1
         # -DTHOTH_DISABLE_SDL1=1
         # -DTHOTH_DISABLE_SDL2=1
make
# make doxygen
# make test
make install # su -c "make install" # sudo make install

Without CMake
-------------

This project is a header-only library, you can copy the include directory in /usr/local (for example) or in your project. (But you have to define some macros to enable optional parts.)


 -------------
| Utilization |
 -------------

If you use CMake, add these lines in your CMakeLists.txt:
# Thōth
message(STATUS "---")
find_package(thoth REQUIRED)
# See /installation/path/lib/thoth/thoth-config.cmake for CMake variables
